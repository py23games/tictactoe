import copy
import random
import sys

import numpy as np
import pygame
from constants import *

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('TIC TAC TOE AI')
screen.fill(BG_COLOR)


class Board:
    def __init__(self):
        self.squares = np.zeros((ROWS, COLS))
        self.empty_squares = self.squares
        self.marked_squares = 0

    def winner(self, show=False):
        for col in range(COLS):
            if self.squares[0][col] == self.squares[1][col] == self.squares[2][col] != 0:
                if show:
                    color = O_COLOUR if self.squares[0][col] == 2 else X_COLOUR
                    iPos = (col * SQUARE_SIZE + SQUARE_SIZE // 2, 20)
                    fPos = (col * SQUARE_SIZE + SQUARE_SIZE // 2, HEIGHT - 20)
                    pygame.draw.line(screen, color, iPos, fPos, LINES_WIDTH)
                return self.squares[0][col]

        for row in range(ROWS):
            if self.squares[0][row] == self.squares[1][row] == self.squares[2][row] != 0:
                if show:
                    color = O_COLOUR if self.squares[row][0] == 2 else X_COLOUR
                    iPos = (20, row * SQUARE_SIZE + SQUARE_SIZE // 2)
                    fPos = (WIDTH - 20, row * SQUARE_SIZE + SQUARE_SIZE // 2)
                    pygame.draw.line(screen, color, iPos, fPos, LINES_WIDTH)
                return self.squares[0][row]

        if self.squares[0][0] == self.squares[1][1] == self.squares[2][2] != 0:
            if show:
                color = O_COLOUR if self.squares[1][1] == 2 else X_COLOUR
                iPos = (20, 20)
                fPos = (WIDTH - 20, HEIGHT - 20)
                pygame.draw.line(screen, color, iPos, fPos, CROSS_WIDTH)
            return self.squares[1][1]

        if self.squares[0][2] == self.squares[1][1] == self.squares[2][0] != 0:
            if show:
                color = O_COLOUR if self.squares[1][1] == 2 else X_COLOUR
                iPos = (20, HEIGHT - 20)
                fPos = (WIDTH - 20, 20)
                pygame.draw.line(screen, color, iPos, fPos, CROSS_WIDTH)
            return self.squares[1][1]

        return 0

    def mark_square(self, row, col, player):
        self.squares[row][col] = player
        self.marked_squares += 1

    def is_empty_square(self, row, col):
        return self.squares[row][col] == 0

    def get_empty_squares(self):
        empty_squares = []
        for row in range(ROWS):
            for col in range(COLS):
                if self.is_empty_square(row, col):
                    empty_squares.append((row, col))
        return empty_squares

    def is_full(self):
        return self.marked_squares == 9

    def is_empty(self):
        return self.mark_squares == 0


class AI:
    def __init__(self, level=1, player=2):
        self.level = level
        self.player = player

    def rndm(self, board):
        empty_squares = board.get_empty_squares()
        index = random.randrange(0, len(empty_squares))

        return empty_squares[index]

    def minimax(self, board, maximizing):
        case = board.winner()

        if case == 1:
            return 1, None

        if case == 2:
            return -1, None

        elif board.is_full():
            return 0, None

        if maximizing:
            max_eval = -10
            best_move = None
            empty_squares = board.get_empty_squares()

            for (row, col) in empty_squares:
                temp_board = copy.deepcopy(board)
                temp_board.mark_square(row, col, 1)

                eval = self.minimax(temp_board, False)[0]

                if eval > max_eval:
                    max_eval = eval
                    best_move = (row, col)

            return max_eval, best_move

        elif not maximizing:
            min_eval = 10
            best_move = None
            empty_squares = board.get_empty_squares()

            for (row, col) in empty_squares:
                temp_board = copy.deepcopy(board)
                temp_board.mark_square(row, col, self.player)

                eval = self.minimax(temp_board, True)[0]

                if eval < min_eval:
                    min_eval = eval
                    best_move = (row, col)

            return min_eval, best_move

    def eval(self, main_board):
        if self.level == 0:
            eval = 'random'
            move = self.rndm(main_board)
        else:
            eval, move = self.minimax(main_board, False)

        print(f'AI moved to {move} with evaluation: {eval}')
        return move


class Game:

    def __init__(self):
        self.board = Board()
        self.ai = AI()
        self.player = 1
        self.gamemode = 'ai'
        self.running = True
        self.draw_lines()

    def draw_lines(self):
        screen.fill(BG_COLOR)

        # horizontal
        pygame.draw.line(screen, LINES_COLOUR, (20, SQUARE_SIZE),
                         (WIDTH - CROSS_WIDTH, SQUARE_SIZE), LINES_WIDTH)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (20, SQUARE_SIZE), LINES_WIDTH/2)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (WIDTH - CROSS_WIDTH, SQUARE_SIZE), LINES_WIDTH/2)

        pygame.draw.line(screen, LINES_COLOUR, (20, 2*SQUARE_SIZE),
                         (WIDTH - CROSS_WIDTH, 2*SQUARE_SIZE), LINES_WIDTH)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (20, 2*SQUARE_SIZE), LINES_WIDTH/2)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (WIDTH - CROSS_WIDTH, 2*SQUARE_SIZE), LINES_WIDTH/2)

        # vertical
        pygame.draw.line(screen, LINES_COLOUR, (SQUARE_SIZE, 20),
                         (SQUARE_SIZE, WIDTH - CROSS_WIDTH), LINES_WIDTH)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (SQUARE_SIZE, 20), LINES_WIDTH/2)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (SQUARE_SIZE, WIDTH - CROSS_WIDTH), LINES_WIDTH/2)

        pygame.draw.line(screen, LINES_COLOUR, (2*SQUARE_SIZE, 20),
                         (2*SQUARE_SIZE, WIDTH - CROSS_WIDTH), LINES_WIDTH)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (2*SQUARE_SIZE, 20), LINES_WIDTH/2)
        pygame.draw.circle(screen, LINES_COLOUR,
                           (2*SQUARE_SIZE, WIDTH - CROSS_WIDTH), LINES_WIDTH/2)

    def draw_figures(self, row, col):
        if self.player == 1:
            pygame.draw.circle(
                screen, O_COLOUR, ((int(col*SQUARE_SIZE+SQUARE_SIZE//2)), (int(row*SQUARE_SIZE+SQUARE_SIZE//2))), CIRCLE_RADIUS, CIRCLE_WIDTH)
        elif self.player == 2:
            pygame.draw.line(screen, X_COLOUR, (col*SQUARE_SIZE+OFFSET,
                                                row*SQUARE_SIZE+SQUARE_SIZE-OFFSET), (col*SQUARE_SIZE+SQUARE_SIZE-OFFSET, row*SQUARE_SIZE+OFFSET), CROSS_WIDTH)
            pygame.draw.line(screen, X_COLOUR, (col*SQUARE_SIZE+OFFSET,
                                                row*SQUARE_SIZE+OFFSET), (col*SQUARE_SIZE+SQUARE_SIZE-OFFSET, row*SQUARE_SIZE+SQUARE_SIZE-OFFSET), CROSS_WIDTH)

    def make_move(self, row, col):
        self.board.mark_square(row, col, self.player)
        self.draw_figures(row, col)
        self.next_turn()

    def next_turn(self):
        self.player = self.player % 2 + 1

    def change_gamemode(self):
        self.gamemode = 'ai' if self.gamemode == 'pvp' else 'pvp'

    def is_over(self):
        return self.board.winner(show=True) != 0 or self.board.is_full()

    def reset(self):
        self.__init__()


def main():

    game = Game()
    b = game.board
    ai = game.ai

    while True:

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_g:
                    game.change_gamemode()

                if event.key == pygame.K_r:
                    game.reset()
                    b = game.board
                    ai = game.ai

                if event.key == pygame.K_0:
                    ai.level = 0

                if event.key == pygame.K_1:
                    ai.level = 1

            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                row = pos[1] // SQUARE_SIZE
                col = pos[0] // SQUARE_SIZE

                if b.is_empty_square(row, col) and game.running:
                    game.make_move(row, col)

                    if game.is_over():
                        game.running = False

        if game.gamemode == 'ai' and game.player == ai.player and game.running:
            pygame.display.update()

            row, col = ai.eval(b)
            game.make_move(row, col)

            if game.is_over():
                game.running = False

        pygame.display.update()


main()
