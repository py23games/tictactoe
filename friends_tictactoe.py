import pygame
import sys
import numpy as np

pygame.init()

WIDTH = 600
HEIGHT = WIDTH
BG_COLOR = (255, 166, 255)
LINES_WIDTH = 23
LINES_COLOUR = (255, 123, 255)
BOARD_ROWS = 3
BOARD_COLS = 3
O_COLOUR = (223, 223, 223)
X_COLOUR = (66, 66, 66)
SQUARE_SIZE = WIDTH // BOARD_ROWS
CIRCLE_RADIUS = SQUARE_SIZE//3
CIRCLE_WIDTH = 15
SPACE = SQUARE_SIZE//4
X_WIDTH = 23


screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('TIC TAC TOE')
screen.fill(BG_COLOR)

board = np.zeros((BOARD_ROWS, BOARD_COLS))
font = pygame.font.SysFont(None, 23)


def draw_lines():
    # horizontal
    pygame.draw.line(screen, LINES_COLOUR, (20, SQUARE_SIZE),
                     (WIDTH - X_WIDTH, SQUARE_SIZE), LINES_WIDTH)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (20, SQUARE_SIZE+1), LINES_WIDTH/1.9)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (WIDTH - X_WIDTH, SQUARE_SIZE+1), LINES_WIDTH/1.9)

    pygame.draw.line(screen, LINES_COLOUR, (20, 2*SQUARE_SIZE),
                     (WIDTH - X_WIDTH, 2*SQUARE_SIZE), LINES_WIDTH)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (20, 2*SQUARE_SIZE+1), LINES_WIDTH/1.9)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (WIDTH - X_WIDTH, 2*SQUARE_SIZE+1), LINES_WIDTH/1.9)

    # vertical
    pygame.draw.line(screen, LINES_COLOUR, (SQUARE_SIZE, 20),
                     (SQUARE_SIZE, WIDTH - X_WIDTH), LINES_WIDTH)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (SQUARE_SIZE+1, 20), LINES_WIDTH/1.9)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (SQUARE_SIZE+1, WIDTH - X_WIDTH), LINES_WIDTH/1.9)

    pygame.draw.line(screen, LINES_COLOUR, (2*SQUARE_SIZE, 20),
                     (2*SQUARE_SIZE, WIDTH - X_WIDTH), LINES_WIDTH)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (2*SQUARE_SIZE+1, 20), LINES_WIDTH/1.9)
    pygame.draw.circle(screen, LINES_COLOUR,
                       (2*SQUARE_SIZE+1, WIDTH - X_WIDTH), LINES_WIDTH/1.9)


def draw_figures():
    for row in range(BOARD_ROWS):
        for col in range(BOARD_COLS):
            if board[row][col] == 1:
                pygame.draw.circle(
                    screen, O_COLOUR, ((int(col*SQUARE_SIZE+SQUARE_SIZE//2)), (int(row*SQUARE_SIZE+SQUARE_SIZE//2))), CIRCLE_RADIUS, CIRCLE_WIDTH)
            elif board[row][col] == 2:
                pygame.draw.line(screen, X_COLOUR, (col*SQUARE_SIZE+SPACE,
                                 row*SQUARE_SIZE+SQUARE_SIZE-SPACE), (col*SQUARE_SIZE+SQUARE_SIZE-SPACE, row*SQUARE_SIZE+SPACE), X_WIDTH)
                pygame.draw.line(screen, X_COLOUR, (col*SQUARE_SIZE+SPACE,
                                 row*SQUARE_SIZE+SPACE), (col*SQUARE_SIZE+SQUARE_SIZE-SPACE, row*SQUARE_SIZE+SQUARE_SIZE-SPACE), X_WIDTH)


def mark_square(row, col, player):
    board[row][col] = player


def available_square(row, col):
    return board[row][col] == 0


def is_board_full():
    for row in range(BOARD_ROWS):
        for col in range(BOARD_COLS):
            if board[row][col] == 0:
                return False
    return True


def check_win(player):
    for col in range(BOARD_COLS):
        if board[0][col] == player and board[1][col] == player and board[2][col] == player:
            draw_vertical_winning_line(col, player)
            return True

    for row in range(BOARD_ROWS):
        if board[row][0] == player and board[row][1] == player and board[row][2] == player:
            draw_horizontal_winning_line(row, player)
            return True

    if board[2][0] == player and board[1][1] == player and board[0][2] == player:
        draw_asc_diagonal(player)
        return True

    if board[0][0] == player and board[1][1] == player and board[2][2] == player:
        draw_desc_diagonal(player)
        return True

    return False


def draw_vertical_winning_line(col, player):
    posX = col * SQUARE_SIZE + SQUARE_SIZE//2

    if player == 1:
        color = O_COLOUR
    elif player == 2:
        color = X_COLOUR

    pygame.draw.line(screen, color, (posX, 15),
                     (posX, HEIGHT - 15), LINES_WIDTH)


def draw_horizontal_winning_line(row, player):
    posY = row * SQUARE_SIZE + SQUARE_SIZE//2

    if player == 1:
        color = O_COLOUR
    elif player == 2:
        color = X_COLOUR

    pygame.draw.line(screen, color, (15, posY),
                     (WIDTH - 15, posY), LINES_WIDTH)


def draw_asc_diagonal(player):
    if player == 1:
        color = O_COLOUR
    elif player == 2:
        color = X_COLOUR

    pygame.draw.line(screen, color, (15, HEIGHT - 15),
                     (WIDTH - 15, 15), LINES_WIDTH)


def draw_desc_diagonal(player):
    if player == 1:
        color = O_COLOUR
    elif player == 2:
        color = X_COLOUR

    pygame.draw.line(screen, color, (15, 15),
                     (WIDTH - 15, HEIGHT - 15), LINES_WIDTH)


def restart():
    screen.fill(BG_COLOR)
    draw_lines()
    for row in range(BOARD_ROWS):
        for col in range(BOARD_COLS):
            board[row][col] = 0


draw_lines()

player = 1
game_over = False

'''mainloop'''
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

        if event.type == pygame.MOUSEBUTTONDOWN and not game_over:
            mouse_x = event.pos[0]
            mouse_y = event.pos[1]

            clicked_row = int(mouse_y // SQUARE_SIZE)
            clicked_col = int(mouse_x // SQUARE_SIZE)

            if available_square(clicked_row, clicked_col):
                mark_square(clicked_row, clicked_col, player)
                if check_win(player) or is_board_full():
                    show_text()
                    game_over = True
                player = player % 2 + 1

                draw_figures()

        def show_text():
            text = font.render("Hit space to restart...", True, X_COLOUR)
            screen.blit(text, [WIDTH/2 - 66, 3])

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                restart()
                game_over = False

    pygame.display.update()
